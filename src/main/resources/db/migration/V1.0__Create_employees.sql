use  employee_db;

CREATE TABLE employees (
    ID BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    age INTEGER,
    gender VARCHAR(255),
    salary FLOAT(53),
    company_id BIGINT,
    PRIMARY KEY (id)
);