# Daily Report (2023/07/20)
# O
### code review
##### In today's code review session, through the teacher's teaching examples and other classmates' assignments, I have basically mastered some techniques for using MOCK. I did not fill the blank space of yesterday afternoon, and also learned about global exception handling in this session.

### SQL
##### I have learned SQL before, so today I will follow the teacher to review the basic operations of adding, deleting, modifying, and querying databases.

### Spring Data JPA
* Define Entity Class: Before using Spring Data JPA, you first need to define an entity class, mark it with the @Entity annotation, and set the mapping relationship between the entity class and the database table.
* Create Repository Interface: Create an interface and inherit JpaRepository, specifying entity classes and their primary key types through generics. This interface will provide us with common CRUD operation methods, such as saving, querying, deleting, etc.
* Custom query method: In the Repository interface, we can define a custom query method through the convention of method names, which should be named according to certain rules. You can refer to the
  Spring Data JPA documentation. In addition, you can also use @Queryannotations to customize query statements.
* Using association relationships: Spring Data JPA supports defining association relationships between entity classes, such as one-to-one relationships, one-to-many relationships, and many to many relationships. Define association relationships by using annotations such as @JoinColumn, @OneToMany, @ManyToOne, etc.
# R
meaningful
# I
The most meaningful activity is Spring Data JPA.
# D
To be used Spring Data JPA in future projects